<!doctype html>
<?php include 'function.php';?>
<!-- simplified doctype works for all previous versions of HTML as well -->
<!-- Paul Irish's technique for targeting IE, modified to only target IE6, applied to the html element instead of body -->
<!--[if IE 6]><html lang="en" class="no-js ie6"><![endif]-->
<!--[if (gt IE 6)|!(IE)]><!--><html lang="it" class="no-js" <?php if(empty($parser['path'])|| in_array($parser['path'], $arrayHome)):?>style="overflow: hidden"<?php endif;?>><!--<![endif]-->

<head>
	<!-- simplified character encoding -->
	<meta charset="utf-8">

	<title>Thinple</title>
	<meta name="description" content="Thinple">
	<meta name="author" content="Stefano">

	<!-- Delete these two icon references once you've placed them in the root directory with these file names -->
	<!-- favicon 16x16 -->
	<link rel="shortcut icon" href="/favicon.ico">
	<!-- apple touch icon 57x57 -->
	<link rel="apple-touch-icon" href="/apple-touch-icon.png">
        
       <!-- The default timeline stylesheet -->
       <link rel="stylesheet" href="timeline/compiled/css/timeline.css" />
       
       	<!-- Main style sheet. Change version number in query string to force styles refresh -->
	<!-- Link element no longer needs type attribute -->
	<link rel="stylesheet" href="css/styles.css?v=1.0">
        <link rel="stylesheet" href="css/Thinple.css?v=1.0">

	<!-- Modernizr for feature detection of CSS3 and HTML5; must be placed in the <head> -->
	<!-- This is a *full-featured* version of Modernizr; you don't need all this -->
	<!-- Go to Modernizr.com/download and build your own -->
	<!-- In HTML5, script tag no longer needs type attribute -->
	<script src="js/modernizr-2.0.6.min.js"></script>

	<!-- Remove the script reference below if you're using Modernizr -->
	<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
        
        
        <!-- Google Maps API -->
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true&language=it"></script>
        
</head>
<?php session_start(); //La sessione comincia?>
<!-- If possible, use the body as the container -->
<!-- The "home" class is an example of a dynamic class created on the server for page-specific targeting -->
<body class="home" id="home">
	
	<!-- ******************************************************************** -->
	<!-- The content below is for demonstration of some common HTML5 elements  -->
	<!-- More than likely you'll rip out everything except header/section/footer and start fresh -->
	
	<!-- First header has a unique class so you can give it individual styles -->
	<header class="theader<?php if(isset($_SESSION['username']) or (str_replace("/", "", $parser['path'])=="registrazione.php")) echo " Tlogin"?>" >

		<!-- "hgroup" is used to make two headings into one, to prevent a new document node from forming -->
                <hgroup class="thinpleLogo">
		<h1>Thinple</h1>
		</hgroup>
                
		
                <?php if(empty($parser['path'])|| in_array($parser['path'], $arrayHome)):?>
                    <?php if(!isset($_SESSION['username'])):?>
                
                        <form class="loginForm" method="post">
                            <input name="mail" type="email" placeholder="Inserisci mail" required>
                            <input name="psw" type="password" placeholder="Inserisci Password" required>
                            <button type="submit">Login</button>
                            <div id="loginErrato"></div>
                        </form>
                    <?php else: header('location: profilo.php');?>
                    <?php endif;?>
                <?php else:?>
                    <?php if(!isset($_SESSION['username']) and !(str_replace("/", "", $parser['path'])=="registrazione.php")and !(str_replace("/", "", $parser['path'])=="registra.php") ): header('location:/'); else:?>
                        <?php if(!(str_replace("/", "", $parser['path'])=="registrazione.php")and !(str_replace("/", "", $parser['path'])=="registra.php")):?>    
                            <!-- Main nav; -->
                            <nav>
                                <ul>
                                    <?php $typePage=str_replace("/", "", $parser['path'])?>
                                        <li><a href="profilo.php" <?php if(($typePage=="profilo.php") or ($typePage=="timeline-profilo.php")):?>class="active"<?php endif;?>>Profilo</a></li>
                                        <li><a href="mappa.php" <?php if(($typePage=="mappa.php")):?>class="active"<?php endif;?>>Mappa</a></li>
                                        <li><a href="monumenti.php" <?php if(($typePage=="monumenti.php") or ($typePage=="monumento.php")or ($typePage=="timeline-monumenti.php")):?>class="active"<?php endif;?>>Monumenti</a></li>
                                        <li><a href="amici.php" <?php if(($typePage=="amici.php")or ($typePage=="amico.php")or ($typePage=="timeline-amici.php")):?>class="active"<?php endif;?>>Amici</a></li>
                                        <li><a id="lgout">Log Out</a></li>
                                </ul>
                            </nav>
                        <?php endif;?>
                    <?php endif;?>
                <?php endif;?>
	</header><!-- .hd1 -->