<?php include 'header.php';?>
<!-- This is the main "div" that wraps the content generically; don't use "section" for this -->
	<div class="main">
            <hgroup id="hreg">
                  <h2>REGISTRAZIONE</h2>
             </hgroup>
            <div class="box Registrazione">
                <form action="registra.php" method="POST">
                    <input id="username" name="user" type="email" placeholder="Nome Utente"required>
                    <span id="status"></span>
                    <input name="psw" type="password" placeholder="Passowrd" required>
                    <input name="nickname" type="text" placeholder="Nickname" required>
                    <input name="nome" type="text" placeholder="Nome" required>
                    <input name="cognome" type="text" placeholder="Cognome" required>
                    <input name="dnascita" type="date" placeholder="Data di Nascita" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01])[/](0[1-9]|1[012])[/][0-9]{4}" required="required">
                    <input name="telefono" type="tel" placeholder="N Telefono" required="required" pattern="^([0-9]*\-?\ ?\/?[0-9]*)$">
                    
                    <select name="city" required="required">
                        <option value="">Seleziona Città</option>
                        <option value="3">Milano</option>
                        <option value="4">Roma</option>
                    </select>
                    <select name="gender" required="required">
                        <option value="">Seleziona Sesso</option>
                        <option value="male">Maschio</option>
                        <option value="famale">Femmina</option>
                    </select>
                    <button id="regbtn" type="submit">Registrati</button>
                </form>
            </div>
	</div><!-- .main -->
<?php include 'footer.php';?>  