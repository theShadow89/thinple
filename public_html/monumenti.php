<?php include 'header.php';?>
<!-- Questa Pagina contiene gli ultimi monumenti visitati -->
<?php   $arr = array("data"=>array('content'=>$content,'time'=>"",'location'=>$location,'entity'=>'places'),'token'=>$_SESSION["token"],'graphid'=>$_SESSION["graphid"]);
        $monArr=richiesta_json($arr,"AppResearchMeetings");?>
	<div class="main">
                <!--Informazioni sul profilo-->
                <?php include 'InfoProfilo.php';?>   
                <section class="box AllMonumenti">
                        <hgroup>
                            <h2>
                                I Monumenti che hai visitato
                                <div id="order">Ordina per<a class="link-sort-list data">Data</a><a class="link-sort-list nome">Nome</a></div>
                            </h2>
                        </hgroup>
                   <div class="divmon">
                     <ul id="sort-list" class="ElementsContent">
                        <?php foreach ($monArr["places"]["data"] as $monumento): ?>
                                <?php order_by_field($monumento["meetings"]["meetings"],"meetingtime"); ?>
                                    <li addr="<?php echo $monumento["meetings"]["meetings"][0]["coordinates"];?>" nome="<?php echo $monumento["name"];?>" data="<?php echo date("d/m/Y", strtotime($monumento["meetings"]["meetings"][0]["meetingtime"]));?>">
                                        <a href="monumento.php?id=<?php echo $monumento["graphid"]?>" ><div class="Elemento">
                                                <img src="getPhoto.php?ID=<?php echo $monumento["graphid"]?>&entity=place&type=profilo">
                                                <div class="Info">
                                                    <div class="Nome"><?php echo $monumento["name"];?></div>
                                                    <span class="Luogo"><label>Luogo:</label><?php echo $monumento["meetings"]["meetings"][0]["coordinates"];?></span>
                                                    <span class="Data"><label>Data Ultimo Incontro: </label><?php echo date("d/m/Y", strtotime($monumento["meetings"]["meetings"][0]["meetingtime"]));?></span>
                                                </div>
                                            </div>                        
                                        </a>
                                    </li>  
                        <?php endforeach; ?>
                     </ul>
                 </div>  
                </section><!-- .hs1 -->

	</div><!-- .main -->
<?php include 'footer.php';?>