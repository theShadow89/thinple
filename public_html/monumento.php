<?php include 'header.php';?>
<!-- Questa Pagina contiene i monumenti incontrati -->
<?php   $arr = array("data"=>array('content'=>$content,'time'=>"",'location'=>$location,'entity'=>'places'),'token'=>$_SESSION["token"],'graphid'=>$_SESSION["graphid"]);
        $ArrIncontriMonumenti=richiesta_json($arr,"AppResearchMeetings");
        ?>
	<div class="main">
            <!--Informazioni sul profilo-->
                <?php include 'InfoMonumento.php';?> 
                <section class="box ComMonumento">
                    <hgroup>
                        <h2>Le mie Visite a <?php echo $mon["name"]?></h2>
                    </hgroup>
                     <?php $incontri=array();?>
                    <?php foreach ($ArrIncontriMonumenti["places"]["data"] as $incontroAmico): ?>
                                <?php if($incontroAmico["graphid"]==$_GET["id"]):?>
                                        <?php $incontri=$incontroAmico["meetings"]?>
                                <?php endif;?>
                    <?php endforeach;?>
                    <div class="divcom">
                                <ul class="ElementsContent">
                                    <?php $numeroincntri=1;?>
                                    <?php foreach ($incontri["meetings"] as $incontro): ?>
                                        <li addr="<?php echo $incontro["coordinates"];?>">
                                            <div class="Elemento">
                                                <div class="Info">
                                                    <div class="Nome"><?php echo $numeroincntri?>° Incontro</div>
                                                    <span class="Luogo"><label>Luogo: </label><?php echo $incontro["coordinates"];?></span>
                                                    <span class="Data"><label>Data: </label><?php echo date("d/m/Y", strtotime($incontro["meetingtime"]));?></span>
                                                    <div id=""></div>
                                                </div>
                                            </div>
                                        </li>
                                <?php $numeroincntri++;?>
                            <?php endforeach; ?>
                        </ul>
                    </div>

		</section><!-- .hs1 -->

	</div><!-- .main -->
<?php include 'footer.php';?>