<?php include 'header.php';?>
<!-- Questa Pagina contiene gli amici incontrati -->
<?php   $arr = array("data"=>array('content'=>$content,'time'=>"",'location'=>$location,'entity'=>'people'),'token'=>$_SESSION["token"],'graphid'=>$_SESSION["graphid"]);
        $amiArr=richiesta_json($arr,"AppResearchMeetings");?>
	<div class="main">
            <!--Informazioni sul profilo-->
                <?php include 'InfoProfilo.php';?> 
                <section class="box AllMonumenti">
                        <hgroup>
                        <h2>
                            Gli Amici che hai Incontrato
                            <div id="order">Ordina per<a class="link-sort-list data">Data</a><a class="link-sort-list nome">Nome</a></div>
                        </h2>
                        </hgroup>
                    <div class="divami">
                     <ul id="sort-list" >
                        <?php foreach ($amiArr["people"]["data"] as $amico): ?>
                         <?php order_by_field($amico["meetings"]["meetings"],"meetingtime"); ?>
                                    <li nome="<?php echo $amico["name"];?>" data="<?php echo date("d/m/Y", strtotime($amico["meetings"]["meetings"][0]["meetingtime"]));?>">
                                        <a href="amico.php?id=<?php echo $amico["graphid"]; ?>" ><div class="Elemento">
                                                <img src="image/segnaPimgel.jpg">
                                                <div class="Info">
                                                    <div class="Nome"><?php echo $amico["name"];?></div>
                                                    <span class="Luogo"><label>Luogo: </label><?php echo $amico["meetings"]["meetings"][0]["coordinates"];?></span>
                                                    <span class="Data"><label>Data: </label><?php echo date("d/m/Y", strtotime($amico["meetings"]["meetings"][0]["meetingtime"]));?></span>
                                                </div>
                                            </div>                        
                                        </a>
                                    </li>  
                        <?php endforeach; ?>
                     </ul>
                 </div> 
		</section><!-- .hs1 -->

	</div><!-- .main -->
<?php include 'footer.php';?>