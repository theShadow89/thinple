<?php include 'header.php';?>
<!-- This is the main "div" that wraps the content generically; don't use "section" for this -->
	<div class="main">
                <!--Informazioni sul profilo-->
                <?php include 'InfoProfilo.php';?> 
                <?php
                    $arr = array("data"=>array('content'=>$content,'time'=>"",'location'=>$location,'entity'=>'places,people'),'token'=>$_SESSION["token"],'graphid'=>$_SESSION["graphid"]);
                    $meetingsArr=richiesta_json($arr,"AppResearchMeetings");
                    $meetingPlaces= order_by_field(($meetingsArr["places"]["data"]), "meetingtime");
                    $meetingPeople= order_by_field(($meetingsArr["people"]["data"]), "meetingtime");
                ?>
                <section class="box UltimiMonumenti">
                        <hgroup>
                        <h2>Ultimi monumenti visitati</h2>
                        </hgroup>
                    <div class="contUltMon">
                        <ul class="ElementsContent" >
                            <?php foreach ($meetingPlaces as $monumento): ?>
                            <?php order_by_field($monumento["meetings"]["meetings"],"meetingtime"); ?>
                                        <li addr="<?php echo $monumento["meetings"]["meetings"][0]["coordinates"];?>">
                                            <a href="monumento.php?id=<?php echo $monumento["graphid"]?>" >
                                                <div class="Elemento">
                                                    <img src="getPhoto.php?ID=<?php echo $monumento["graphid"]?>&entity=place&type=profilo">
                                                    <div class="Info">
                                                        <div class="Nome"><?php echo $monumento["name"];?></div>
                                                        <span class="Luogo"><label>Luogo: </label><?php echo $monumento["meetings"]["meetings"][0]["coordinates"];?></span>
                                                        <span class="Data"><label>Data: </label><?php echo date("d/m/Y", strtotime($monumento["meetings"]["meetings"][0]["meetingtime"]));?></span>
                                                        <div id=""></div>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                            <?php endforeach; ?>
                        </ul>
                    </div> 
		</section><!-- .hs1 -->
                
                <section class="box UltimiAmici">
                       <hgroup>
                        <h2>Ultimi amici incontrati</h2>
                    </hgroup>
                    <div class="contUltAmi">
                        <ul class="ElementsContent">
                            <?php foreach ($meetingPeople as $amico): ?>
                            <?php order_by_field($amico["meetings"]["meetings"],"meetingtime"); ?>
                                    <li addr="<?php echo $amico["meetings"]["meetings"][0]["coordinates"];?>">
                                        <a href="amico.php?id=<?php echo $amico["graphid"]?>" >
                                            <div class="Elemento">
                                                <img src="getPhoto.php?ID=<?php echo $amico["graphid"]?>&entity=people&type=profilo">
                                                <div class="Info">
                                                    <div class="Nome"><?php echo $amico["name"];?></div>
                                                    <span class="Luogo"><label>Luogo: </label><?php echo $amico["meetings"]["meetings"][0]["coordinates"];?></span>
                                                    <span class="Data"><label>Data: </label><?php echo date("d/m/Y", strtotime($amico["meetings"]["meetings"][0]["meetingtime"]));?></span>
                                                    <div id=""></div>
                                                </div>  
                                            </div>
                                        </a>
                                    </li>
                        <?php endforeach; ?>
                        </ul>
                    </div>
		</section><!-- .hs1 -->

	</div><!-- .main -->
<?php include 'footer.php';?>