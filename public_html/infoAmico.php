<!-- Informazioni relative al prfilo chevengono visualizzate nelle pagine generali -->
<?php   $arr = array("data"=>array('content'=>$content,'location'=>$location,'entity'=>'people'),'token'=>$_SESSION["token"],'graphid'=>$_SESSION["graphid"]);
        $amiArr=richiesta_json($arr,"AppResearchAll");
        $ami=array();
        foreach ($amiArr["people"]["data"] as $amico ){
            if($amico["graphid"]==$_GET["id"]){
                //Se esiste il monumento recupero l' array contenente le sue informazioni
                $ami=$amico;
            }                
        }
        ?> 
<div class="contentMainProfiloAmico">
    <div class="ContentAmico">
                    <!-- Qursta sezione contiene  la foto del profilo-->
		<section class="box FotoAmico">
                    <img src="getPhoto.php?ID=<?php echo $amico["graphid"]?>&entity=people&type=profilo">
		</section>
                    <!-- Qursta sezione contiene  Nome del Profilo-->
                <section style="float: left">
                    <?php echo $ami["name"]?>
                </section>
                <!-- Qursta sezione contiene  informazioni sul profilo-->
                <section class="box InfoAmico" >
                    <div id="mapFriend">
                        
                        </div>
                    </section>
                    </div>
		
    </div>
    <?php $arr = array("data"=>array('content'=>$content,'time'=>"",'location'=>$location,'entity'=>'people'),'token'=>$_SESSION["token"],'graphid'=>$_SESSION["graphid"]);
          $ArrIncontriAmici=richiesta_json($arr,"AppResearchMeetings");
   ?>
    <section class="box UltimiIncontri">
        <hgroup>
            <h2>I Nostri Incontri</h2>
        </hgroup>
        <div class="contUltMon">
            <?php $incontri=array();?>
            <?php foreach ($ArrIncontriAmici["people"]["data"] as $incontroAmico): ?>
                                <?php if($incontroAmico["graphid"]==$_GET["id"]):?>
                                        <?php $incontri=$incontroAmico["meetings"]?>
                                <?php endif;?>
            <?php endforeach;?>
            <?php order_by_field($amico[$incontri],"meetingtime"); ?>
                        <ul class="ElementsContent">
                                <?php $numIncontri=1;?>
                                    <?php foreach ($incontri["meetings"] as $incontro): ?>
                                        <li addr="<?php echo $incontro["coordinates"];?>" coord="<?php echo $incontro["coordinates"];?>">
                                            <div class="Elemento">
                                                <img src="image/segnaPimgel.jpg">
                                                <div class="Info">
                                                    <div class="Nome"><?php echo  $numIncontri?>° Incontro</div>
                                                    <span class="Luogo"><label>Luogo: </label><?php echo $incontro["coordinates"];?></span>
                                                    <span class="Data"><label>Data: </label><?php echo date("d/m/Y", strtotime($incontro["meetingtime"]));?></span>
                                                    <div id=""></div>
                                                </div>
                                            </div>
                                        </li>
                                <?php $numIncontri++;?>
                            <?php endforeach; ?>
                        </ul>
                    </div> 
    </section>
</div>