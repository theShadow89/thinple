<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
include 'globalVar.php';
//Array conenente i possibili percorsi della home del sito
$arrayHome=array("/",
          "index.php");
if(isset($_SERVER['REQUEST_URI']))
    $parser=  parse_url($_SERVER['REQUEST_URI']);



function richiesta_json($arr,$app){
    global $CH;
    $json = json_encode($arr);
    $json=  str_replace("\/","/",$json);   
        //inizializzo la richiesta
        $ch= curl_init($CH.$app);
        
	//eseguo la richiesta
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); //richiesta tramite post
	curl_setopt($ch, CURLOPT_POSTFIELDS, $json); // stringa con la richiesta json
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($json))
	);
	
	if( ! $result = curl_exec($ch))
	{
	$json = json_encode($arr);
	
		echo 'errore: ' . curl_error($ch);
		trigger_error(curl_error($ch));//intercetta eventuali errori
	}
	
	curl_close($ch);
	$arr_res=json_decode($result, true);
        return $arr_res;
}


function order_by_field($array,$field){
    # inizializziamo un ciclo for che abbia come condizione di terminazione
# il numero degli array interni meno "1"
for ($i=0;$i<count($array)-1;$i++)
{
  # inizializziamo un ciclo for che abbia come condizione di terminazione
  # il numero degli array
  for ($j=$i+1;$j<count($array);$j++)
  {
    # utilizziamo come indici i valori derivanti dall'iterazione dei cicli e utilizziamoli
    # per effettuare un controllo tra valori 
    $ordina = strcmp($array[$i][$field], $array[$j][$field]);
    # ordiniamo i valori sulla base dei confronti ponendo per primi
    # i valori alfabeticamente "maggiori" 
    if ($ordina > 0)
    {
      $ordinato = $array[$i];
      $array[$i] = $array[$j];
      $array[$j] = $ordinato;
    }
  }
}
return $array;
}
//@ return the tag img with base64 encode
function get_extension($ID,$entity,$type){
    if(file_exists('image/'.$entity.'/'.$ID.'/'.$type.'.jpg'))
            $extension='jpg';
    else if(file_exists('image/'.$entity.'/'.$ID.'/'.$type.'.png'))
            $extension='png';
    else if(file_exists('image/'.$entity.'/'.$ID.'/'.$type.'.gif'))    
            $extension='gif';    
    return $extension;
}
    
?>