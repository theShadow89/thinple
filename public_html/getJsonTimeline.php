<?php
include "function.php";
session_start();
// Array rappresentante l' insieme di tutti gli elementi sulla linea del tempo
$datesArray=array();

//Questo array rappresenta un singolo elemento sulla linea del tempo
$date=array("startDate"=>"",
             "headline"=>"",
             "text"=>"Descrizione",
             "asset"=>array(
                       "media"=>"http://".$_SERVER['SERVER_NAME']."/image/default.jpg",
                        "credit"=>"",
                         "caption"=>""
                        )
    );

if(isset($_SESSION["type"])){
        if($_SESSION["type"]=="prf"){
            $entity = 'places,people';
            $headline='Timeline Profilo';
            $text='Incontri con amici e monumenti';
        }
        if($_SESSION["type"]=="mon"){
            $entity = 'places';
            $headline='Timeline Monumenti';
            $text='Incontri con i monumenti';
        }
        if($_SESSION["type"]=="ami"){
            $entity = 'people';
            $headline='Timeline Amici';
            $text='Incontri con gli amici';
        }

        $arr = array("data"=>array('content'=>$content,'location'=>$location,'time'=>"",'entity'=>$entity),'token'=>$_SESSION["token"],'graphid'=>$_SESSION["graphid"]);
        $Arr=richiesta_json($arr,"AppResearchMeetings");
        $ind=0;
        $places=$Arr["places"]["data"];
        
        foreach ($places as $place){
            foreach ($place["meetings"]["meetings"] as $incontro){
                //creo elemento timeline solo se place è stato visitato
                $date["headline"]="<a href='monumento.php?id=".$place["graphid"]."'>".$place["name"]."</a>";
                $date["startDate"]=date("Y,m,d", strtotime($incontro["meetingtime"]));
                $date["asset"]["media"]='http://'.$_SERVER['SERVER_NAME'].'/getPhoto.php?ID='.$place["graphid"].'&entity=place&type=profilo';
                $datesArray[$ind]=$date;
                $ind++;
            }
        }
        
        $people=$Arr["people"]["data"];
        foreach ($people as $person){
            foreach ($person["meetings"]["meetings"] as $incontro){
                $date["headline"]="<a href='amico.php?id=".$person["graphid"]."'>".$person["name"]."</a>";
                $date["startDate"]=  date("Y,m,d", strtotime($incontro["meetingtime"]));
                $date["asset"]["media"]='http://'.$_SERVER['SERVER_NAME'].'/getPhoto.php?ID='.$place["graphid"].'&entity=people&type=profilo';
                $datesArray[$ind]=$date;
                $ind++;
            }
        }

        $pr=array("timeline"=>array("headline"=>$headline,"type"=>"default","text"=>$text,"startDate"=>date("Y"),
            "date"=>$datesArray));
            $date=str_replace("\/","/",json_encode($pr));
            print $date;
}    
?>