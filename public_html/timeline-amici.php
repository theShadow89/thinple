<?php include 'header.php';
//setto la variabile type a ami per poter ottenere i dati necessari ad ottenere la timeline degli amici
$_SESSION["type"]="ami";
?>
<!-- This is the main "div" that wraps the content generically; don't use "section" for this -->
	<div class="main">
                <!--Informazioni sul profilo-->
                <?php include 'InfoProfilo.php';?> 
                <section>
                       <div id="timeline"></div>
		</section><!-- .hs1 -->

	</div><!-- .main -->
<?php include 'footer.php';?>