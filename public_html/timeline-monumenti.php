<?php include 'header.php';
//setto la variabile type a mon per poter ottenere i dati necessari ad ottenere la timeline dei monumenti
$_SESSION["type"]="mon";
?>
<!-- This is the main "div" that wraps the content generically; don't use "section" for this -->
	<div class="main">
                <!--Informazioni sul profilo-->
                <?php include 'InfoProfilo.php';?> 
                <section>
                       <div id="timeline"></div>
		</section><!-- .hs1 -->

	</div><!-- .main -->
<?php include 'footer.php';?>