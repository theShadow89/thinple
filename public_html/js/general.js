/*globals $ */

// The script below does essentially nothing other than define some nearly empty methods and a few settings for demonstration purposes
// You can delete this whole thing, or do whatever you want with it

// variable for caching settings
var s = null,

	PrimaryNameSpace = {
		// define your oft-used settings below
		settings: {
			basicExample: $('.main'),
			nestedExample: {
				first: true,
				second: true,
				third: true,
				fourth: true,
				fifth: ['one', 'two', 'three', 'four', 'five', 'six']
			},

			foo: 'bar'
		},

		// the method that initializes stuff
		init: function () {
			/*	the line below can be included in each method to reference the settings 
				without always having to type "this.settings" each time */
			s = this.settings;
			// using firebug you can view all settings with "console.log(s)";

			// after you do stuff here, you can call the next method
			// You can use "this" in the current context to reference "PrimaryNameSpace" directly
			this.nextMethod();

		},

		nextMethod: function () {
			s = this.settings;
			// do stuff here
		},

		anotherMethod: function () {
			s = this.settings;
			// do more stuff here
		}

		// remember not to use a trailing comma after the last method is defined; you could leave a dummy method here to prevent that error
	};

// The section below initializes the whole thing; you could pass in some JSON data or some other object that needs to be worked with, and call individual methods.

$(function () {
    //This function  inzialize the single place map
    function initializePM(){
        var coord=$("#coord").attr("name");
        var coorArr=coord.split(" ");
        var long=coorArr[1];
        var lat=coorArr[0];
        var nome_Monumento=$("#coord").attr("mon");
        var latlng = new google.maps.LatLng(lat, long);
            var myOptions = { 
            zoom: 13,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            
            var mymap= new google.maps.Map(document.getElementById('mapBox'), myOptions);
            
            var marker = new google.maps.Marker({
            position: latlng,
            map: mymap, title: nome_Monumento});
    }
    //This function  inzialize global map
    function initializeMG(){
        var latlng = new google.maps.LatLng(0,0);
            var myOptions = { 
            zoom: 3,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            
            var map= new google.maps.Map(document.getElementById('mapGeneral'), myOptions);
            if ($("#mapData").length > 0){
                $("#mapData>li").each(function(){
                var coord=$(this).attr("coord");
                var img=$(this).attr("image");
                var link=$(this).attr("link");
                var friend=$(this).attr("friend");
                var coorArr=coord.split(" ");
                var lat=coorArr[1];
                var long=coorArr[0];
                var nome_Monumento=$(this).attr("nome");
                var latlng = new google.maps.LatLng(lat, long);
                var icon;
                if(friend=="1")
                    icon="http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png";
                else
                    icon="http://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png";
                
                var contentString = '<div><a style="display:block; color: #595959; text-decoration: none; font-size: 13px;" href="'+link+'">'+nome_Monumento+'</a><div><img style=" height: 52px; width: 52px;" src="'+img+'"></div></div>';

                var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });

                
                
                var marker = new google.maps.Marker({
                    position: latlng,
                    map: map, 
                    title: nome_Monumento,
                    icon:icon
                        
                });
                
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open(map,marker);
                });
                
                 var currentAddress;

                                     //Evento che gestisce lo zoom anche su pressione tasto Invio
                                    $("#gaddress").keyup(function(e) {
						$("#gload").click();
     
                                    });
                                
                                    $("#gaddress").click(function(){
                                        currentAddress = $(this).val();
                                        if(currentAddress != '')
                                            $("#gaddress").val('');
                                    });

                                    //Evento che gestisce lo zoom su click tasto Load    
                                    $("#gload").click(function(){
                                            if($("#gaddress").val() != '') {                                
                                            currentAddress = $("#gaddress").val();
                                            geoc(currentAddress,map);
                                        }
                                    });
                
             });                           
         }
}

function initializeFM(){
            var coord=$(".ElementsContent li").attr("coord"); 
            var coorArr=coord.split(" ");
            var lat=coorArr[1];
            var long=coorArr[0];
            var center = new google.maps.LatLng(lat,long);
            var myOptions = { 
            zoom: 13,
            center: center,
            mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            
            var map= new google.maps.Map(document.getElementById('mapFriend'), myOptions);
            if ($(".contUltMon").length > 0){
                $(".ElementsContent>li").each(function(){
                    console.log($(this));
                coord=$(this).attr("coord");
                coorArr=coord.split(" ");
                lat=coorArr[1];
                long=coorArr[0];
                
                //var nome_Monumento=$(this).attr("nome");
                var latlng = new google.maps.LatLng(lat, long);
                var icon="http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png";

                
                //var contentString = '<div><a style="display:block; color: #595959; text-decoration: none; font-size: 13px;" href="'+link+'">'+nome_Monumento+'</a><div><img style=" height: 52px; width: 52px;" src="'+img+'"></div></div>';

               /** var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });**/

                
                
                var marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    icon:icon
                        
                });
                
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open(map,marker);
                });
             });                           
         }
}


    //this function reverse the coordnate into address
    function initializeGeocode(){
               geocoder = new google.maps.Geocoder();
               if($(".MappaMon").children("#coord").length > 0){
                   var coordMon=$(".MappaMon").children("#coord").attr("name");
                   var coordMonArr=coordMon.split(" ");
                   getAddr(coordMonArr[1],coordMonArr[0],$(".InfoMonumento"));
               }
                $(".ElementsContent>li").each(function(){
                var coord=$(this).attr("addr");
                var coorArr=coord.split(" ");
                var lat=coorArr[0];
                var long=coorArr[1];
                getAddr(lat,long,$(this));
                });
}

function geoc(address,map) {

               var geocoder = new google.maps.Geocoder();

               if (geocoder) {

		geocoder.geocode({"address": address}, function(results, status) {

                 if (status == google.maps.GeocoderStatus.OK) {
		//Se ritornano le coordinate centra la mappa in esse ad uno zoom di 15
                 map.setCenter(results[0].geometry.location); 
                 map.setZoom(14);                                                                                    
		}
	});

       }
   
   }

function getAddr (lat,long,el) {
                        var check=false;
                        console.log(lat+" "+long);
                        var latlng = new google.maps.LatLng(lat, long);
                        geocoder.geocode({'latLng': latlng}, function(results, status) {
                                    if (status == google.maps.GeocoderStatus.OK) {
                                        var address=results[0].formatted_address;                                      
                                        if(el.children(".contentInfo").length > 0){
                                            var addresses=results[0];                                        
                                            
                                            for (var i = 0; i < addresses.address_components.length; i++)
                                            {
                                                console.log(addresses.address_components[i]);
                                                
                                                if(($.inArray("locality",addresses.address_components[i].types)!=-1) || (($.inArray("administrative_area_level_3",addresses.address_components[i].types)!=-1) && $.inArray("political",addresses.address_components[i].types)!=-1)){
                                                    if(!check){
                                                        var città=addresses.address_components[i].long_name;
                                                        check=true;
                                                    }
                                                    else{
                                                        città+=", "+addresses.address_components[i].long_name; 
                                                        }
                                                    console.log(addresses.address_components[i].long_name);                                                   
                                                 }
                                                else if(($.inArray("route",addresses.address_components[i].types)!=-1))
                                                     var via=addresses.address_components[i].long_name
                                            }
                                            el.children(".contentInfo").children("#InfoPCittà").html("<label>Città: </label>"+città);
                                            el.children(".contentInfo").children("#InfoPVia").html("<label>Via: </label>"+via);
                                        }
                                        else{
                                            el.attr("addr",address);
                                            //se c'è il tag a allora devo considerlarlo quando recupero i figli dell' elemento attuale
                                            if(el.children("a").length > 0)
                                               el.children("a").children(".Elemento").children(".Info").children(".Luogo").html("<label>Luogo: </label>"+address);
                                           else
                                               el.children(".Elemento").children(".Info").children(".Luogo").html("<label>Luogo: </label>"+address);                                  
                                            }
                                    }
                          });
}
    
    if ($("#coord").length > 0){
    google.maps.event.addDomListener(window, "load", initializePM);
    }
   google.maps.event.addDomListener(window, "load", initializeMG);
   google.maps.event.addDomListener(window, "load", initializeFM);
   google.maps.event.addDomListener(window, "load", initializeGeocode);
	PrimaryNameSpace.init();
	PrimaryNameSpace.anotherMethod();
        
        /* attach a submit handler to the form */
        $(".loginForm").submit(function(event) {

            /* fermo l' invio del form */
            event.preventDefault();
            
            $("#loginErrato").remove("#errLog");
            
            /* get some values from elements on the page: */
            var $form = $( this ),
            u = $form.find( 'input[name="mail"]' ).val(),
            p = $form.find( 'input[name="psw"]' ).val();
            
            /* Send the data using post and put the results in a div */
            $.post( 'login.php', { username: u ,password: p },
            function(data) {
                //parso il risultato json per ottenere i valori
                var pdata=$.parseJSON(data);
                console.log(pdata.tipo);
                if(pdata.tipo==="errore"){
                    $(".loginForm")[0].reset();
                    $("#loginErrato").append("<p id=errLog>L' Username o la Password non sono corretti. Riprova!</p>");
                }else{
                    window.location.replace("profilo.php");
                }
            }
            );
        });
        
        $("#username").change(function() 
        { 
            var username = $("#username").val();
            var msgbox = $("#status");

            if(username.length > 3)
            {
                $("#status").html('&nbsp;Controllo Disponibilit&agrave;...');
 
                $.ajax({ 
                type: "POST", 
                url: "check_user.php", 
                data: "username="+ username, 
                success: function(msg){ 
                    $("#status").ajaxComplete(function(event, request){ 
 
                        if(msg == 'OK'){ 
                            // if you don't want background color remove these following two lines
                            $("#username").removeClass("red"); // remove red color
                            $("#username").addClass("green"); // add green color
                            msgbox.html(' <img src="../image/greenCheck.gif" width="20px">');
                        } 
                        else { 
                            // if you don't want background color remove these following two lines
                            $("#username").removeClass("green"); // remove green color
                            $("#username").addClass("red"); // add red  color
                            msgbox.html(msg);
                            } 
                    });
                  } 
               }); 
        }
        else{
            // if you don't want background color remove this following line
            $("#username").addClass("red"); // add red color
            $("#status").html('<font color="#cc0000">Inserisci un valido Username</font>');
        }
        return false;
    });
    if ($(".divmon ul li").length > 0){
        //Carousel per i monumenti visualizza gli utlimi 7 visitati
        $(".divmon").jCarouselLite({
            vertical:true,
            mouseWheel: true,
            circular: false,
            visible: 7
        });
    }

    if ($(".divami ul li").length > 0){
        //Carousel per gli amici visualizza gli utlimi 7 incontrati
        $(".divami").jCarouselLite({
            vertical:true,
            mouseWheel: true,
            circular: false,
            visible: 7
        });
    }

    if ($(".divcom ul li").length > 0){
        //Carousel per gli ultimi commenti
        $(".divcom").jCarouselLite({
            vertical:true,
            mouseWheel: true,
            circular: false,
            visible: 3
        });
    }

    if ($(".contUltMon ul li").length > 0){
    //Carousel per gli utlimi monumenti visualizza gli utlimi 3 visitati
    $(".contUltMon").jCarouselLite({
        vertical:true,
        mouseWheel: true,
        circular: false,
        visible: 3
    });    
    }
    if ($(".contUltAmi ul li").length > 0){
    //Carousel per gli utlimi amici visualizza gli utlimi 3 incontrati
    $(".contUltAmi").jCarouselLite({
        vertical:true,
        mouseWheel: true,
        circular: false,
        visible: 3
    });
    }
    
        $("#lgout").click(function(){
        console.log("click");
        $.post('logout.php',function(data) {
                //parso il risultato json per ottenere i valori                
                    window.location.replace("index.php");
        });
    });
    
    $(".link-sort-list").click(function(e){
      var $sort = this;
    var $list = $('#sort-list');
    var $listLi = $('li',$list);
    console.log($sort);
    if($($sort).hasClass('nome')){
        var type="nome";
        console.log(type);
    }else{
        var type='data';
    }
    $listLi.sort(function(a, b){
        var keyA = $(a).attr(type);
        var keyB = $(b).attr(type);        
        console.log(keyA);
        return (keyA > keyB) ? 1 : 0;
    });
    $.each($listLi, function(index, row){
        $list.append(row);
    });
    e.preventDefault();
    });

    //timeline load
    if ($("#timeline").length > 0){
        createStoryJS({
            lang:'it',
            iwidth:'800',
            height:'600',
            source:'getJsonTimeline.php',
            embed_id:'timeline',
            start_at_end:true
	});
    }

     
});