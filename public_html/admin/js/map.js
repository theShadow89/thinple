$(function () {
        function initializeMap(){
                
                var hasLocation = false;
                var latlng = new google.maps.LatLng(0.0,0.0);
                
                var myOptions = {
                zoom: 3,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            
            var map= new google.maps.Map(document.getElementById('geolocation-map'), myOptions);
            
            var marker = new google.maps.Marker({
						map: map

				 });
                
                                
                                
                                     var currentAddress;

                                     //Evento che gestisce lo zoom anche su pressione tasto Invio
                                    $("#gaddress").keyup(function(e) {
						$("#gload").click();
     
                                    });
                                
                                    $("#gaddress").click(function(){
                                        currentAddress = $(this).val();
                                        if(currentAddress != '')
                                            $("#gaddress").val('');
                                    });

                                    //Evento che gestisce lo zoom su click tasto Load    
                                    $("#gload").click(function(){
                                            if($("#gaddress").val() != '') {                                
                                            currentAddress = $("#gaddress").val();
                                            geoc(currentAddress);
                                        }
                                    });
                                    
                                    google.maps.event.addListener(map, 'click', function(event) {
						placeMarker(event.latLng);
					});
                                    
                                        
                                        function placeMarker(location) {

						marker.setPosition(location);

						map.setCenter(location);

						if((location.lat() != '') && (location.lng() != '')) {

							$("#geolocation-latitude").val(location.lat());

							$("#geolocation-longitude").val(location.lng());

						}
                                            
                                          }
                    
                    

					function geoc(address) {

						var geocoder = new google.maps.Geocoder();

                                                    if (geocoder) {

							geocoder.geocode({"address": address}, function(results, status) {

                                                        if (status == google.maps.GeocoderStatus.OK) {
								    //Se ritornano le coordinate centra la mappa in esse ad uno zoom di 15
                                                                    map.setZoom(14);
								    placeMarker(results[0].geometry.location);   
                                                                        
            
								}
							});

                                                    }

						$("#geodata").html(latitude + ',' + longitude);

					}
                                    
                                       function reverseGeocode(location) {

                                            var geocoder = new google.maps.Geocoder();
                                                console.log("prova");
					    if (geocoder) {

							geocoder.geocode({"latLng": location}, function(results, status) {

							if (status == google.maps.GeocoderStatus.OK) {

							  if(results[1]) {

							  	var address = results[1].formatted_address;

							  	if(address == "")

							  		address = results[7].formatted_address;

							  	else {

									$("#gaddress").val(address);

									placeMarker(location);

							  	}

							  }

							}

							});

						}

					}
                                    
 }
       google.maps.event.addDomListener(window, "load", initializeMap());
});