  <!DOCTYPE html>
 <?php $server=parse_url($_SERVER['SERVER_NAME']);?>
  <?php include '../function.php';?>  
    <html>
    <head>
    <title>Thinple Admin</title>
    <!-- Bootstrap -->
    <!-- Force latest IE rendering engine or ChromeFrame if installed -->
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
    <meta charset="utf-8">
    <title>jQuery File Upload Demo</title>
    <meta name="description" content="File Upload widget with multiple file selection, drag&amp;drop support, progress bar and preview images for jQuery. Supports cross-domain, chunked and resumable file uploads. Works with any server-side platform (Google App Engine, PHP, Python, Ruby on Rails, Java, etc.) that supports standard HTML form file uploads.">
    <meta name="viewport" content="width=device-width">
    <!-- Bootstrap CSS Toolkit styles -->
    <link rel="stylesheet" href="http://blueimp.github.com/cdn/css/bootstrap.min.css">
    <!-- Generic page styles -->
    <link rel="stylesheet" href="http://<?=$server["path"]?>/admin/upload/css/style.css">
    <!-- Bootstrap styles for responsive website layout, supporting different screen sizes -->
    <link rel="stylesheet" href="http://blueimp.github.com/cdn/css/bootstrap-responsive.min.css">
    <!-- Bootstrap CSS fixes for IE6 -->
    <!--[if lt IE 7]><link rel="stylesheet" href="http://blueimp.github.com/cdn/css/bootstrap-ie6.min.css"><![endif]-->
    <!-- Bootstrap Image Gallery styles -->
    <link rel="stylesheet" href="http://blueimp.github.com/Bootstrap-Image-Gallery/css/bootstrap-image-gallery.min.css">
    <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
    <link rel="stylesheet" href="http://<?=$server["path"]?>/admin/css/jquery.fileupload-ui.css">
    <!-- CSS adjustments for browsers with JavaScript disabled -->
    <noscript><link rel="stylesheet" href="css/jquery.fileupload-ui-noscript.css"></noscript>
    <!-- Shim to make HTML5 elements usable in older Internet Explorer versions -->
    <!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <link href="http://<?=$server["path"]?>/admin/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="http://<?=$server["path"]?>/admin/bootstrap/css/style.css" rel="stylesheet" media="screen">
    </head>
    <body>
 <?php $user="prova"; if(isset($user)):?>
         <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="http://<?=$server["path"]?>/admin/index.php">Thinple</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
              <li class="dropdown">
                  <a href="http://<?=$server["path"]?>/admin/place.php" data-toggle="dropdown" class="dropdown-toggle">Places <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="http://<?=$server["path"]?>/admin/newplace.php">Aggiungi Place</a></li>
                  <li><a href="http://<?=$server["path"]?>/admin/upload/index.php">Upload File</a></li>
                </ul>
              </li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>
        <?php endif; ?>