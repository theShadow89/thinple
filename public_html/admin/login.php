<?php
include 'function.php';
if(isset($_POST["username"])and isset($_POST["password"])){
// prelevo username e password dal POST,
// eliminando eventuali spazi alle estremità delle stringhe
$username = trim($_POST["username"]);
$password = trim($_POST["password"]);

// elimino eventuali backslash inseriti automaticamente da PHP
if(get_magic_quotes_gpc())
{
	$username = stripslashes($username);
	$password = stripslashes($password);
}

      $arr = array('mail' => $username,'password'=>$password);
      $array=richiesta_json($arr,"AppLogin");      
      
// provo ad effettuare il login con i dati recuperati dal form
$esito = login($_POST['username'],$_POST['password'],$array);

if($esito['tipo']=="successo"){
    session_start();
    $_SESSION['username']=$array["data"]["usurname"];
    $_SESSION['graphid']=$array["data"]["graphid"];
    $_SESSION["birthday"]=$array["data"]["birthday"];
    $_SESSION["countryid"]=$array["data"]["countryid"];
    $_SESSION["cityid"]=$array["data"] ["cityid"];
    $_SESSION["gender"]=$array["data"]["gender"];
    $_SESSION['token']=$array["data"]["token"];
}
// invio la risposta al client
echo json_encode($esito);
}
// funzione per la verifica dei dati di login
function login($username, $password,$array)
{
        $esito=array();
	$username_corretto = $array['mail'];
	$password_corretta = $array['data']['password'];
        
	if(!$username || !$password) {
		return $esito=array("tipo"=>"errore");
	}

	if($username != $username_corretto) {
		return $esito=array("tipo"=>"errore");
	}

	if($username == $username_corretto && $password != $password_corretta) {
		return $esito=array('tipo'=>'errore');
	}

	if($username == $username_corretto && $password == $password_corretta) {
                return $esito=array('tipo'=>'successo');
	}
}
?>