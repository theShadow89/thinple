<?php include 'header.php';?>
<?php $arr = array("data"=>array('content'=>$content,'location'=>$location,'entity'=>'places'),'token'=>'','graphid'=>'ddd');
      $placeArr=richiesta_json($arr,"AppResearchAll"); 
      ?>
    <div class="container">
        <table class="table">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Coordinates</th>
                  <th>Edit/Delete</th>
                </tr>
                <?php foreach ($placeArr["places"]["data"] as $place): ?>
                <tr>
                    <td><?php echo $place["graphid"] ?></td>
                    <td><?php echo $place["name"] ?></td>
                    <td><?php echo $place["coordinates"] ?></td>
                    <td><a href="edit">Edit</a> <a href="delete">Delete</a></td>
                </tr>
                <?php endforeach;?>
              </thead>
        </table>
            
    </div> <!-- /container -->
<?php include 'footer.php';?>
