<?php include 'header.php';
//setto la variabile type a prf per poter ottenere i dati necessari ad ottenere la timeline del profilo
$_SESSION["type"]="prf";
?>
<!-- This is the main "div" that wraps the content generically; don't use "section" for this -->
	<div class="main">
                <!--Informazioni sul profilo-->
                <?php include 'InfoProfilo.php';?> 
                <section>
                       <div id="timeline"></div>
		</section><!-- .hs1 -->

	</div><!-- .main -->
<?php include 'footer.php';?>