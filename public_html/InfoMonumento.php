<!-- Informazioni relative al prfilo chevengono visualizzate nelle pagine generali -->
<?php   $arr = array("data"=>array('content'=>$content,'time'=>"",'location'=>$location,'entity'=>'places'),'token'=>$_SESSION["token"],'graphid'=>$_SESSION["graphid"]);
        $monArr=richiesta_json($arr,"AppResearchMeetings");
        $mon=array();
        foreach ($monArr["places"]["data"] as $monumento ){
            if($monumento["graphid"]==$_GET["id"]){
                //Se esiste il monumento recupero l' array contenente le sue informazioni
                $mon=$monumento;
            }                
        }
        ?> 
<div class="contentMainProfilo">
    <div class="ContentMonumento">
                    <!-- Qursta sezione contiene  la foto del profilo-->
		<section class="box FotoMonumento">
                     <img src="getPhoto.php?ID=<?php echo $_GET["id"]?>&entity=place&type=profilo">
		</section>
                    <!-- Qursta sezione contiene  Nome del Profilo-->
                <section style="float: left">
                    <?php echo $mon["name"]?>
                </section>
                    <!-- Qursta sezione contiene  informazioni sul profilo-->
                <section class="box MappaMon" >
                    <div id="mapBox"></div>
                    <div id="coord" name="<?php echo $mon["meetings"]["meetings"][0]["coordinates"] ?>" mon="<?php echo $mon["name"];?>"></div>
		</section>
    </div>              
    <div class="box InfoMonumento">
        <div class="contentInfo">
                        <div id="InfoPCittà">
                            <label>Città: </label><?php echo $mon["cityid"]?></div>
                        <div id="InfoPVia">
                            <label>Via: </label><?php echo $mon["via"]?></div> 
                        <div id="InfoDVis">
                            <?php if(isset($mon["meetings"]["meetings"][0]["meetingtime"])):?>
                                <label>Ultima Visita il: </label><?php echo date("d/m/Y", strtotime($mon["meetings"]["meetings"][0]["meetingtime"]));?>
                            <?php else:?>
                               <label>Non Ancora Visitato</label>
                            <?php endif;?>
                        </div>
         </div>
    </div>
</div>