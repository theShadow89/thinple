        <!-- Like the main header, the main footer has a class for its own unique styles -->
        <footer class=<?php if(!isset($_SESSION['username']) and !(str_replace("/", "", $parser['path'])=="registrazione.php")) echo 'LogOutFooter'  ?>>
            <p>copyright &copy; <?php echo date("Y");?></p>
	</footer><!-- .f1 -->

<!-- Remote jQuery with local fallback; taken from HTML5 Boilerplate http://html5boilerplate.com -->
<!-- jQuery version might not be the latest; check jquery.com -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"></script>
<script>!window.jQuery && document.write(unescape('%3Cscript src="js/jquery-1.6.4.min.js"%3E%3C/script%3E'))</script>


<script type="text/javascript" src="js/jcarousellite.js"></script>
<script type="text/javascript" src="js/mousewheel.js"></script>
<script src="timeline/compiled/js/storyjs-embed.js"></script>
<!-- Below is your script file, which has a basic JavaScript design pattern that you can optionally use -->
<!-- Keep this and plugin scripts at the bottom for faster page load; combining and minifying scripts is recommended -->
<script src="js/general.js"></script>

<!-- asynchronous analytics code by Mathias Bynens; change UA-XXXXX-X to your own code; http://mathiasbynens.be/notes/async-analytics-snippet -->
<!-- this can also be placed in the <head> if you want page views to be tracked quicker -->
<script>
var _gaq = [['_setAccount', 'UA-XXXXX-X'], ['_trackPageview']];
(function(d, t) {
	var g = d.createElement(t),
		s = d.getElementsByTagName(t)[0];
	g.async = true;
	g.src = ('https:' == location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	s.parentNode.insertBefore(g, s);
})(document, 'script');
</script>
</body>
</html>