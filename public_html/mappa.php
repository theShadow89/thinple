<?php include 'header.php';?>
<!-- Questa Pagina contiene gli ultimi monumenti visitati -->
<?php   $arr = array("data"=>array('content'=>$content,'location'=>$location,'entity'=>'places,people'),'token'=>$_SESSION["token"],'graphid'=>$_SESSION["graphid"]);
        $monArr=richiesta_json($arr,"AppResearchAll"); ?>

	<div class="main">
                <!--Informazioni sul profilo-->
                <?php include 'InfoProfilo.php';?>   
                <section class="box AllMonumenti"> 
                    <hgroup>
                            <h2>Mappa Generale
                                    <div id="MapFind">
                                        <input type="text" id="gaddress" name="gaddress" class="newtag form-input-tip" size="25" autocomplete="off" value="" />
                                        <input id="gload" type="button" class="button geolocationadd" value="Cerca" tabindex="3" />
                                        <input type="hidden" id="geolocation-latitude" name="geolocation-latitude" />
                                        <input type="hidden" id="geolocation-longitude" name="geolocation-longitude" />
                                    </div>
                             </h2>
                     </hgroup>
                   <div class="divmap">
                       <div id="mapGeneral"></div>
                     <ul id="mapData">
                        <?php foreach ($monArr["places"]["data"] as $monumento): ?>
                                    <li nome="<?php echo$monumento["name"]?>" coord="<?php echo $monumento["coordinates"]?>" link="monumento.php?id=<?php echo $monumento["graphid"]?>" image="<?php echo 'getPhoto.php?ID='.$monumento["graphid"].'&entity=place&type=profilo'?>" friend="<?php echo $monumento["friend"]?>">
                                    </li>
                        <?php endforeach; ?>
                     </ul>
                 </div>  
                </section><!-- .hs1 -->

	</div><!-- .main -->
<?php include 'footer.php';?>